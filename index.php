<!DOCTYPE HTML>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">

	<title>Your Website</title>
	<link rel="stylesheet" href="public/css/main.min.css?07" type="text/css" />
</head>

<body>

	<header>
		<div class="navbar-menu">
            <div class="menu_main_line">
                <div class="menu_left-block">
                    <a class="navbar-menu_btn js-modal-btn" data-modal="menuModal"></a>
                    <a href="#header" class="navbar-menu_logo soft">
                        <object type="image/svg+xml" data="public/img/LOGO/LOGO.svg" >
                            <img src="public/img/LOGO/LOGO.png" alt="" />
                        </object>
                    </a>

                    <div class="menu_main_line_nav sm-hide">
                        <ul>
                            <li><a href="#" class="soft-link">Главная</a> </li>
                            <li><a href="#art-block-3" class="soft-link">Секции</a> </li>
                            <li><a href="#art-block-5" class="soft-link">Курсы</a> </li>
                            <li><a href="#art-block-8" class="soft-link">Стоимость обучения</a> </li>
                            <li><a href="#art-block-11" class="soft-link">Тренеры</a> </li>
                            <li><a href="#art-block-14" class="soft-link">Контакты</a> </li>
                        </ul>
                    </div>
                </div>
            </div>


            <div class="contact-modal_tabs contact-modal_tabs_menu">
                <div class="contact-modal_tabs_ico ico-phone active" data-tab="phone"></div>
                <div class="contact-modal_tabs_ico ico-mail" data-tab="mail"></div>
                <div class="contact-modal_tabs_ico ico-map" data-tab="map"></div>
            </div>

		</div>
	</header>

    <div class="modal fade" id="menuModal" tabindex="-1" role="dialog" >
        <div class="modal-content container nav-main-menu">
            <div class="modal-header">
                <div class="menu_left-block">
                    <a class="navbar-menu_btn js-modal-close" data-modal="menuModal"></a>
                    <a href="#header" class="navbar-menu_logo soft">
                        <object type="image/svg+xml" data="public/img/LOGO/LOGO.svg" >
                            <img src="public/img/LOGO/LOGO.png" alt="" />
                        </object>
                    </a>
                </div>
                <button type="button" class="close modal-close js-modal-close" data-modal="menuModal"></button>
            </div>

            <div class="row sm-content">
                <div class="col-sm-6">
                    <ul>
                        <li><a href="#art-block-1" class="soft-link">01_ Главная </a></li>
                        <li><a href="#art-block-2" class="soft-link">02_ Что получает ребенок? </a></li>
                        <li><a href="#art-block-3" class="soft-link">03_ Секции </a></li>
                        <li><a href="#art-block-4" class="soft-link">04_ Почему к нам? </a></li>
                        <li><a href="#art-block-5" class="soft-link">05_ Курсы </a></li>
                        <li><a href="#art-block-6" class="soft-link">06_ Результаты обучения </a></li>
                        <li><a href="#art-block-7" class="soft-link">07_ Как стать нашим студентом? </a></li>
                        <li><a href="#art-block-8" class="soft-link">08_ Сколько стоит обучение? </a></li>
                        <li><a href="#art-block-9" class="soft-link">09_ Отзывы </a></li>
                        <li><a href="#art-block-10" class="soft-link">10_ Запись на бесплатное занятие </a></li>
                        <li><a href="#art-block-11" class="soft-link">11_ Наши тренеры </a></li>
                        <li><a href="#art-block-12" class="soft-link">12_ Фотографии с занятий </a></li>
                        <li><a href="#art-block-13" class="soft-link">13_ У вас есть ВОПРОСЫ? </a></li>
                        <li><a href="#art-block-14" class="soft-link">14_ Контакты </a></li>
                    </ul>
                </div>
                <div class="col-sm-6">
                    <a class="btn btn-red">Записаться на пробное занятие</a>
                    <div class="menu_main_contact">
                        <p>Центр развития "Первое Дело"</p>
                        <p class="txt-grey">Адрес:</p>
                        <p>ул. Достоевского, 48 Казань Респ. Татарстан
                            42009</p>
                        <p class="txt-grey">Телефон:</p>
                        <p>+7 (843) 333 75 85</p>
                        <p class="txt-grey">E-mail:</p>
                        <p>pervoedelo@gmail.com</p>
                        <p>Центр работает в будни, с 9:00 до 18:00</p>
                    </div>
                    <div class="social-block">
                        <div class="social-block_item"><i class="ico ico-fb"></i></div>
                        <div class="social-block_item"><i class="ico ico-inst"></i></div>
                        <div class="social-block_item"><i class="ico ico-tw"></i></div>
                        <div class="social-block_item"><i class="ico ico-yb"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal contact-modal-win hide" id="contactModal" tabindex="-1" role="dialog" >
        <div class="modal-content container contact-modal">
            <div class="modal-header">
                <button type="button" class="close modal-close js-modal-contact-close" data-modal="contactModal"></button>
            </div>
            <div class="contact-modal_tabs">
                <div class="contact-modal_tabs_ico ico-phone active" data-tab="phone"></div>
                <div class="contact-modal_tabs_ico ico-mail" data-tab="mail"></div>
                <div class="contact-modal_tabs_ico ico-map" data-tab="map"></div>
            </div>
            <div class="contact-modal_content_blocks">
                <div class="contact-modal_content contact-modal_content_phone open">
                    <h2>Обратный звонок</h2>
                    <p class="txt-grey">Представьтесь, мы вам перезвоним.</p>
                    <div class="form_block">
                        <form class="form feedbackForm js-form row">
                            <div class="col-sm-12 control"><input type="text" placeholder="Имя*"></div>
                            <div class="col-sm-12 control"><input type="text" placeholder="Телефон*"></div>
                            <div class="col-sm-12 control">
                                <label class="row">
                                    <div class="checkbox_input"><input type="checkbox" class="regular-checkbox" checked></div>
                                    <p class="checkbox_txt">Я согласен с  условиями обработки персональных данных</p>
                                </label>
                            </div>
                            <div class="col-sm-12 control">
                                <button class="btn btn-red">Отправить</button>
                            </div>
                        </form>
                    </div>

                </div>
                <div class="contact-modal_content contact-modal_content_mail">
                    <h2>Запись на бесплатное занятие</h2>
                    <div class="form_block">
                        <form class="form feedbackForm js-form row">
                            <div class="col-sm-12 control"><input type="text" placeholder="Имя*"></div>
                            <div class="col-sm-6 control"><div class="offset"><input type="email" placeholder="E-mail*"></div></div>
                            <div class="col-sm-6 control"><div class="offset"><input type="text" placeholder="Телефон*"></div></div>
                            <div class="col-sm-12 control control-line">
                                <label class="row">
                                    <div class="checkbox_input"><input type="checkbox" class="regular-checkbox" checked></div>
                                    <p class="checkbox_txt">Я согласен с  условиями обработки персональных данных</p>
                                </label>
                            </div>
                            <div class="col-sm-12 control">
                                <button class="btn btn-red">Отправить</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="contact-modal_content contact-modal_content_map">
                    <div class="">
                        <h2>Контакты</h2>
                        <p>Центр развития "Первое Дело"</p>
                        <p class="txt-grey">Адрес:</p>
                        <p>ул. Достоевского, 48 Казань Респ. Татарстан
                            42009</p>
                        <p class="txt-grey">Телефон:</p>
                        <p>+7 (843) 333 75 85</p>
                        <p class="txt-grey">E-mail:</p>
                        <p>pervoedelo@gmail.com</p>
                        <p>Центр работает в будни,<br> с 9:00 до 18:00</p>
                    </div>
                    <div class="contact-modal_map">
                        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A96acf0bced4a4f4bf602b8f6493e9d12ecac2ed9a7eec6453ddd0273e56451ed&amp;height=300&amp;lang=ru_RU&amp;scroll=true"></script>
                    </div>
                </div>
            </div>


        </div>

    </div>

	<section class="se-container">
	
		<article id="art-block-1" class="art-block-1">
            <div class="sm-image"></div>
			<div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <h1>Хотите быть уверены<br>
                            в будущем своего ребенка?</h1>
                        <p class="h1_descr">Приходите в нашу школу<br class="sm-hide"> юных предпринимателей!</p>
                        <a class="btn btn-red js-open-win" data-tab="mail">Записаться на пробное занятие</a>
                    </div>
                </div>
            </div>
		</article>

        <article id="art-block-2" class="art-block-2">
            <div class="container">
                <h2>Что получает ребенок? </h2>
                <p class="h2_descr">
                    Это интересные занятия в форме тренингов и наставничества, которые помогут ребятам определиться с любимым делом и дадут знания и навыки, необходимые для успеха в бизнесе и в жизни. Занятия делятся на четыре основных блока:
                </p>
                <div class="row item-ico-list">
                    <div class="row">
                        <div class="col-sm-6">
                            <div><div class="item-ico-list_ico ico-1"></div></div>
                            <div class="item-ico-list_text">
                                <p class="item-ico-list_text_title">гармоничные взаимоотношения</p>
                                <p class="item-ico-list_text_descr">учимся понимать других людей<br>
                                    и правильно общаться</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div><div class="item-ico-list_ico ico-2 blue"></div></div>
                            <div class="item-ico-list_text">
                                <p class="item-ico-list_text_title">Личная эффективность</p>
                                <p class="item-ico-list_text_descr">ребенок разовьет внимание,<br> мышление и память</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div><div class="item-ico-list_ico ico-3 blue"></div></div>
                            <div class="item-ico-list_text">
                                <p class="item-ico-list_text_title">Бизнес и финансы</p>
                                <p class="item-ico-list_text_descr">ребенок освоит финансовую<br> грамотность и основы<br> предпринимательства</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div><div class="item-ico-list_ico ico-4"></div></div>
                            <div class="item-ico-list_text">
                                <p class="item-ico-list_text_title">Раскрытие талантов и проф.ориентирование</p>
                                <p class="item-ico-list_text_descr">ребенок получит возможность<br> проявить и развить свои таланты и <br>склонности</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>

        <article id="art-block-3" class="art-block-3">
            <div class="container">
                <div class="row art-block-3_title">
                    <div class="col-sm-6 ">
                        <h2 class="art-block-3_title_left">Секции<span></span></h2>
                    </div>
                    <div class="col-sm-6 ">
                        <p class="art-block-3_title_right">Программа каждого курса адаптирована под возраст студентов. Занятия проводятся один раз в неделю с сентября по май. Для максимальной эффективности обучения в группу зачисляется не более 12 человек. </p>
                    </div>
                </div>
            </div>
                <div class="row">
                    <div class="js-slikc-slider-1 slider-popup">

                        <div class="slide">
                            <img alt="foto" src="public/img/foto/141031-OSX7UU-375.jpg">
                                <div class="slider-popup_descr">
                                    <p class="title">Коммуникации</p>
                                    <p class="txt">Ребенок научится успешно общаться
                                        в разных ситуациях и сферах жизни. Разрешать конфликтные ситуации.
                                        Отстаивать свою точку зрения и противостоять негативному влиянию.
                                        Выступать на публике и проводить переговоры. </p>
                                </div>
                        </div>
                        <div class="slide">
                            <img alt="foto" src="public/img/foto/146734-OTNL17-510.jpg">
                            <div class="slider-popup_descr">
                                <p class="title">Коммуникации2</p>
                                <p class="txt">Ребенок научится успешно общаться
                                    в разных ситуациях и сферах жизни. Разрешать конфликтные ситуации.
                                    Отстаивать свою точку зрения и противостоять негативному влиянию.
                                    Выступать на публике и проводить переговоры. </p>
                            </div>
                        </div>
                        <div class="slide">
                            <img alt="foto" src="public/img/foto/146809-OTNLSF-107.jpg">
                            <div class="slider-popup_descr">
                                <p class="title">Коммуникации 3</p>
                                <p class="txt">Ребенок научится успешно общаться
                                    в разных ситуациях и сферах жизни. Разрешать конфликтные ситуации.
                                    Отстаивать свою точку зрения и противостоять негативному влиянию.
                                    Выступать на публике и проводить переговоры. </p>
                            </div>
                        </div>
                        <div class="slide">
                            <img alt="foto" src="public/img/foto/146881-OTNM5Y-528.jpg">
                        </div>
                        <div class="slide">
                            <img alt="foto" src="public/img/foto/149601-OTYRCC-987.jpg">
                        </div>
                        <div class="slide">
                            <img alt="foto" src="public/img/foto/149807-OTYRSH-72.jpg">
                        </div>
                    </div>
                </div>
            <div class="container container_bg">
                <div class="row sm-hide">
                    <div class="col-sm-6">
                        <div class="content content-l">
                            <p class="txt-type-title">Почему это нравится родителям?</p>
                            <p class="txt-type-ico">успехи в школе</p>
                            <p class="txt-type-1">Развивается мышление и память. Ребенок  лучше усваивает и запоминает новую информацию. Появляется мотивация
                                к учебе.</p>
                            <p class="txt-type-ico">Управление временем и деньгами</p>
                            <p class="txt-type-1">Ребенок учится планировать и больше успевает. Ставит цели и расставляет приоритеты. Осваивает финансовую грамотность.</p>
                            <p class="txt-type-ico">Уверенность в будущем</p>
                            <p class="txt-type-1">Ребенок сможет выбрать дело себе по душе и научится зарабатывать на этом.</p>
                            <p class="txt-type-ico">Развивающее окружение</p>
                            <p class="txt-type-1">Мощный толчок к развитию и постоянная поддержка.</p>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="content content-r">
                            <p class="txt-type-title">Почему к нам?</p>
                            <p class="txt-type-ico">Эффективность обучения</p>
                            <p class="txt-type-1">Формат тренингов, бизнес-игр
                                и мастер-классов позволит  закрепить новые знания и перевести в навыки.</p>
                            <p class="txt-type-ico">Всестороннее развитие</p>
                            <p class="txt-type-1">Мы не знаем, кем станет наш студент, поэтому не только "прокачиваем" бизнес-часть, а помогаем стать успешным в любой сфере.</p>
                            <p class="txt-type-ico">Практика</p>
                            <p class="txt-type-1">Придётся выполнять домашние задания,
                                но мы обещаем: это будет интересно!</p>
                            <p class="txt-type-ico">Профориентация</p>
                            <p class="txt-type-1">Работа с психологом, раскрытие талантов
                                и определение истинного пути. Погружение
                                в профессии.</p>
                        </div>
                    </div>
                </div>
            </div>
        </article>

        <article id="art-block-4" class="art-block-4 sm-hide">

            <div class="tab-menu-block row">

                <div class="multi-tab_left">
                    <ul class="multi-tab_tab-menu">
                        <li class="tab1 active">Курс 1  (возраст 7-8 лет)</li>
                        <li class="tab2">Курс 2  (возраст 9-11 лет)</li>
                        <li class="tab3">Курс 3  (возраст 12-15 лет)</li>
                        <li class="tab4">Курс 4  (возраст 15-16 лет)</li>
                    </ul>
                </div>
                <div class="multi-tab_right">
                    <div class="multi-tab_sm">
                        <ul class="multi-tab_hor-menu">
                            <li class="hor1 active">Коммуникации</li>
                            <li class="hor2">Личная эффективность</li>
                            <li class="hor3">Бизнес и финансы</li>
                            <li class="hor4">Раскрытие талантов и проф.ориентирование</li>
                        </ul>
                    </div>

                    <div class="multi-tab_contents">
                        <div id="tab1" class="multi-tab-content active">
                            <div class="tab-hor-content hor1 active">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p class="txt-grey">
                                            Ребенок научится успешно общаться с разными людьми (в семье, с друзьями, с учителями и пр.). Научится владеть своим голосом, мимикой и жестами. Поймет, когда нужно отстаивать свою точку зрения, а когда нужно уступить. Узнает, почему происходят конфликты, как их избежать и разрешить. Научится организовывать работу команды и принимать совместные решения.
                                        </p>
                                        <ul>
                                            <li>Вербальные и невербальные способы общения</li>
                                            <li>Принципы успешной коммуникации</li>
                                            <li>Разрешение конфликтов. Управление эмоциями</li>
                                            <li>Навыки обсуждения и принятия решений
                                                в команде</li>
                                        </ul>

                                        <a class="btn btn-border js-open-win" data-tab="mail">Записаться на пробное занятие</a>

                                    </div>
                                    <div class="col-sm-6">
                                        <img src="public/img/pic/children-2.jpg">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-hor-content hor2">tab1 hor2 text</div>
                            <div class="tab-hor-content hor3">tab1 hor3 text</div>
                            <div class="tab-hor-content hor4">tab1 hor4 text</div>
                        </div>
                        <div id="tab2" class="multi-tab-content">
                            <div class="tab-hor-content hor1 active">tab2 hor1 text</div>
                            <div class="tab-hor-content hor2">tab2 hor2 text</div>
                            <div class="tab-hor-content hor3">tab2 hor3 text</div>
                            <div class="tab-hor-content hor4">tab2 hor4 text</div>
                        </div>
                        <div id="tab3" class="multi-tab-content">
                            <div class="tab-hor-content hor1 active">tab3 hor1 text</div>
                            <div class="tab-hor-content hor2">tab3 hor2 text</div>
                            <div class="tab-hor-content hor3">tab3 hor3 text</div>
                            <div class="tab-hor-content hor4">tab3 hor4 text</div>
                        </div>
                        <div id="tab4" class="multi-tab-content">
                            <div class="tab-hor-content hor1 active">tab4 hor1 text</div>
                            <div class="tab-hor-content hor2">tab4 hor2 text</div>
                            <div class="tab-hor-content hor3">tab4 hor3 text</div>
                            <div class="tab-hor-content hor4">tab4 hor4 text</div>
                        </div>
                    </div>
                </div>
            </div>

        </article>

        <article id="art-block-5" class="art-block-5 sm-hide">
            <div class="container container_bg virtual-table">
                <div class="table-row">
                    <div class="table-cell"><h2>Основные причины нежелания учиться</h2></div>
                    <div class="table-cell"><h2>Результаты обучения</h2></div>
                </div>
                <div class="table-row">
                    <div class="table-cell smile smile-bed">Функциональные особенности (слабая память,  неумение переключаться с одной деятельности на другую, трудности с концентрацией и пр.)</div>
                    <div class="table-cell smile smile-good">Школьный материал будет усваиваться легче и запоминаться лучше</div>
                </div>
                <div class="table-row">
                    <div class="table-cell smile smile-bed">Трудности при адаптации в коллективе</div>
                    <div class="table-cell smile smile-good">Ребенок научится находить общий язык и успешно общаться</div>
                </div>
                <div class="table-row">
                    <div class="table-cell smile smile-bed">Непонимание, для чего это нужно</div>
                    <div class="table-cell smile smile-good">Через осознание своих целей появится мотивация и интерес к учебе</div>
                </div>
                <div class="table-row">
                    <div class="table-cell smile smile-bed">Страх потерпеть неудачу, внутренние комплексы</div>
                    <div class="table-cell smile smile-good">Появится уверенность в себе, исчезнет страх перед выступлениями, разовьются лидерские навыки </div>
                </div>
                <div class="table-row">
                    <div class="table-cell smile smile-bed">Отвлекающие факторы (гаждеты, компьютерные игры, телевизор), большая нагрузка в школе</div>
                    <div class="table-cell smile smile-good">Ребенок научится расставлять приоритеты и правильно распределять свое время. Станет больше успевать и меньше нервничать</div>
                </div>
            </div>
        </article>

        <article id="art-block-6" class="art-block-6">
            <div class="container">
                <h2>Как стать нашим студентом?</h2>
                <div class="row">
                    <div class="icons-row">
                        <div class="col-sm-3">
                            <div class="icons-row_icon">
                                <object type="image/svg+xml" data="public/img/icons/icons-contact.svg" >
                                    <img src="public/img/icons/contact.png" alt="" />
                                </object>
                            </div>
                            <div class="icons-row_descr">
                                <p class="icons-row_title">заполнить заявку</p>
                                <p class="icons-row_txt">займёт меньше минуты</p>
                                <a href="#" class="btn btn-blue js-open-win" data-tab="mail">Заполнить</a>
                            </div>

                        </div>
                        <div class="col-sm-3">
                            <div class="icons-row_icon">
                                <object type="image/svg+xml" data="public/img/icons/icons-people.svg" >
                                    <img src="public/img/icons/telemarketer.png" alt="" />
                                </object>
                            </div>
                            <div class="icons-row_descr">
                                <p class="icons-row_title">Звонок от менеджера</p>
                                <p class="icons-row_txt">Ответим на ваши вопросы<br>
                                    и запишем на пробное<br> занятие</p>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="icons-row_icon">
                                <object type="image/svg+xml" data="public/img/icons/icons-bootle.svg" >
                                    <img src="public/img/icons/laboratory.png" alt="" />
                                </object>
                            </div>
                            <div class="icons-row_descr">
                                <p class="icons-row_title">Пробное занятие</p>
                                <p class="icons-row_txt">Попробуйте бесплатно<br> и примите решение</p>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="icons-row_icon">
                                <object type="image/svg+xml" data="public/img/icons/icons-hand.svg" >
                                    <img src="public/img/icons/handshake.png" alt="" />
                                </object>
                            </div>
                            <div class="icons-row_descr">
                                <p class="icons-row_title">Заключаем договор</p>
                                <p class="icons-row_txt"> Начало обучения - сентябрь<br> 2018 года</p>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </article>

        <article id="art-block-7" class="art-block-7">
            <div class="container">
                <h2>Сколько стоит обучение?</h2>
                <table class="">
                    <tr>
                        <th colspan="3">Что входит в цену</th>
                    </tr>
                    <tr>
                        <td>Занятия 8 ак.часов в месяц</td>
                        <td><i class="plus"></i></td>
                        <td><i class="plus"></i></td>
                    </tr>
                    <tr>
                        <td>Личный наставник для развития собственного проекта</td>
                        <td><i class="plus"></i></td>
                        <td><i class="plus"></i></td>
                    </tr>
                    <tr>
                        <td>Психологическое сопровождение, помощь и поддержка </td>
                        <td><i class="plus"></i></td>
                        <td><i class="plus"></i></td>
                    </tr>
                    <tr>
                        <td>Методические пособия, пройденный материал в краткой и понятной форме</td>
                        <td><i class="plus"></i></td>
                        <td><i class="plus"></i></td>
                    </tr>
                    <tr>
                        <td>Домашние задания с обратной связью от кураторов</td>
                        <td><i class="plus"></i></td>
                        <td><i class="plus"></i></td>
                    </tr>
                    <tr>
                        <td>Вкусные и полезные какао-брейки</td>
                        <td><i class="plus"></i></td>
                        <td><i class="plus"></i></td>
                    </tr>
                    <tr>
                        <td>Классные призы за активную работу</td>
                        <td><i class="plus"></i></td>
                        <td><i class="plus"></i></td>
                    </tr>
                    <tr>
                        <td>Текст</td>
                        <td><i class="fail"></i></td>
                        <td><i class="plus"></i></td>
                    </tr>
                    <tr>
                        <td class="uppercase">Стоимость обучения</td>
                        <td><span class="nowrap"><span class="txt-big">3 900</span><br class="sm-show"> руб/мес</span></td>
                        <td><span class="nowrap"><span class="txt-big">5 200</span><br class="sm-show"> руб/мес</span></td>
                    </tr>
                </table>
            </div>
        </article>

        <article id="art-block-8" class="art-block-8 sm-hide">
            <div class="container">
                <h2>Отзывы</h2>
                <div class="row review-block">
                    <div class="col-sm-4">
                        <div class="review-block_img"><img src="public/img/pic/review-ico-1.png"></div>
                        <div class="review-block_name">Берл Лазар</div>
                        <div class="review-block_txt txt-grey"> "Немало революций знала Россия, но самая тихая и эффективная - это революция, которую сотворили сторонники ХАБАДа"</div>
                    </div>
                    <div class="col-sm-4">
                        <div class="review-block_img"><img src="public/img/pic/review-ico-2.png"></div>
                        <div class="review-block_name">Лейба Бронштейн (Троцкий)</div>
                        <div class="review-block_txt txt-grey"> "Мы должны превратить Россию в пустыню, населенную белыми неграми, которым мы дадим такую тиранию, которая не снилась никогда самым страшным деспотам Востока. Если мы выиграем революцию, раздавим Россию, то на погребальных обломках ее укрепим власть сионизма и станем такой силой, перед которой весь мир опустится на колени" </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="review-block_img"><img src="public/img/pic/review-ico-3.png"></div>
                        <div class="review-block_name">Анатолий Чубайс (Сагал)</div>
                        <div class="review-block_txt txt-grey">
                            "Что вы волнуетесь за этих людей? Ну, вымрет тридцать миллионов. Они не вписались в рынок. Не думайте об этом - новые вырастут"
                        </div>
                    </div>
                </div>
            </div>
        </article>

        <article id="art-block-9" class="art-block-9">
            <div class="container ">
                <div class="form_block">
                    <form class="form feedbackForm js-form row">
                        <h2>Запись на бесплатное занятие</h2>
                        <div class="col-sm-12 control"><input type="text" placeholder="Имя*"></div>
                        <div class="col-sm-6 control"><div class="offset"><input type="email" placeholder="E-mail*"></div></div>
                        <div class="col-sm-6 control"><div class="offset"><input type="text" placeholder="Телефон*" class="js-phone-mask"></div></div>
                        <div class="col-sm-12 control">
                            <label class="row">
                                <div class="checkbox_input"><input type="checkbox" class="regular-checkbox" checked></div>
                                <p class="checkbox_txt">Я согласен с  <a href="#">условиями обработки персональных данных</a></p>
                            </label>
                        </div>
                        <div class="col-sm-12 control txt-center">
                            <button class="btn btn-red">Записаться на пробное занятие</button>
                        </div>
                    </form>
                </div>
            </div>
        </article>

        <article id="art-block-10" class="art-block-10 sm-hide">
            <div class="container">
                <h2>Наши тренеры</h2>
                <div class="colors-block">
                    <div class="row">
                        <div class="colors-block_image">
                            <img src="public/img/pic/people_review-1.jpg" alt="people 1">
                        </div>
                        <div class="colors-block_description">
                            <p class="colors-block_title">Светлана Иванова</p>
                            <p>Толерантность в медицине — иммунологическое состояние организма, при котором он не способен синтезировать антитела в ответ на введение определённого антигена при сохранении иммунной реактивности к другим антигенам. Проблема иммунологической толерантности имеет значение при пересадке органов и тканей. Полная толерантность — это смерт</p>
                        </div>
                    </div>
                </div>
                <div class="colors-block">
                    <div class="row">
                        <div class="colors-block_image">
                            <img src="public/img/pic/people_review-2.jpg" alt="people 1">
                        </div>
                        <div class="colors-block_description">
                            <p class="colors-block_title">Александр Петров</p>
                            <p>
                                Толерантность в медицине — иммунологическое состояние организма, при котором он не способен синтезировать антитела в ответ на введение определённого антигена при сохранении иммунной реактивности к другим антигенам. Проблема иммунологической толерантности имеет значение при пересадке органов и тканей. Полная толерантность — это смерт</p>                        </div>
                    </div>
                </div>
                <div class="colors-block">
                    <div class="row">
                        <div class="colors-block_image">
                            <img src="public/img/pic/people_review-3.jpg" alt="people 1">
                        </div>
                        <div class="colors-block_description">
                            <p class="colors-block_title">Светлана Иванова</p>
                            <p>Толерантность в медицине — иммунологическое состояние организма, при котором он не способен синтезировать антитела в ответ на введение определённого антигена при сохранении иммунной реактивности к другим антигенам. Проблема иммунологической толерантности имеет значение при пересадке органов и тканей. Полная толерантность — это смерт</p>
                        </div>
                    </div>
                </div>
                <div class="colors-block">
                    <div class="row">
                        <div class="colors-block_image">
                            <img src="public/img/pic/people_review-4.jpg" alt="people 1">
                        </div>
                        <div class="colors-block_description">
                            <p class="colors-block_title">Александр Петров</p>
                            <p>
                                Толерантность в медицине — иммунологическое состояние организма, при котором он не способен синтезировать антитела в ответ на введение определённого антигена при сохранении иммунной реактивности к другим антигенам. Проблема иммунологической толерантности имеет значение при пересадке органов и тканей. Полная толерантность — это смерт</p>                        </div>
                    </div>
                </div>
            </div>
        </article>

        <article id="art-block-11" class="art-block-11">
            <div class="">
                <h2>Фотографии с занятий</h2>
                <div class="js-slikc-slider">
                    <div class="slide">
                        <img alt="foto" src="public/img/foto/141031-OSX7UU-375.jpg">
                    </div>
                    <div class="slide">
                        <img alt="foto" src="public/img/foto/146734-OTNL17-510.jpg">
                    </div>
                    <div class="slide">
                        <img alt="foto" src="public/img/foto/146809-OTNLSF-107.jpg">
                    </div>
                    <div class="slide">
                        <img alt="foto" src="public/img/foto/146881-OTNM5Y-528.jpg">
                    </div>
                    <div class="slide">
                        <img alt="foto" src="public/img/foto/149601-OTYRCC-987.jpg">
                    </div>
                    <div class="slide">
                        <img alt="foto" src="public/img/foto/149807-OTYRSH-72.jpg">
                    </div>
                </div>
            </div>
        </article>

        <article id="art-block-12" class="art-block-12">
            <div class="container">
                <h2>У вас есть ВОПРОСЫ?</h2>
                <div class="accordion">
                    <div class="accordion_item">
                        <div class="accordion_title">
                            <div class="title">Как быть со школьной программой, успеет ли ребенок и там и там?</div>
                        </div>
                        <div class="accordion_description">Всем этим навыкам мы обучаем на наших занятиях. Весь процесс проходит в виде игры - ребенок с удовольствием вовлекается и забывает о том, что он "некоммуникабельный".</div>
                    </div>
                    <div class="accordion_item">
                        <div class="accordion_title">
                            <div class="title">Мой ребенок не комуникабелен. Сможет ли он договариваться, продавать, продвигать свой бизнес?</div>
                        </div>
                        <div class="accordion_description">Всем этим навыкам мы обучаем на наших занятиях. Весь процесс проходит в виде игры - ребенок с удовольствием вовлекается и забывает о том, что он "некоммуникабельный".</div>
                    </div>
                    <div class="accordion_item">
                        <div class="accordion_title">
                            <div class="title">Не повредит ли психику ребенка резкое погружение в жизненные реалии?</div>
                        </div>
                        <div class="accordion_description">3Всем этим навыкам мы обучаем на наших занятиях. Весь процесс проходит в виде игры - ребенок с удовольствием вовлекается и забывает о том, что он "некоммуникабельный".</div>
                    </div>
                </div>
            </div>
        </article>

        <article id="art-block-13" class="art-block-13">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6"><h2>У вас остались вопросы?</h2></div>
                    <div class="col-sm-6 ">
                        <form class="inline-form feedbackForm js-form">
                            <input type="text" placeholder="Телефон">
                            <button class="btn">Заказать звонок</button>
                        </form>
                    </div>
                </div>
            </div>
        </article>

        <article id="art-block-14" class="art-block-14">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Контакты</h2>
                        <p>Центр развития "Первое Дело"</p>
                        <p class="txt-grey">Адрес:</p>
                        <p>ул. Достоевского, 48 Казань Респ. Татарстан
                            42009</p>
                        <p class="txt-grey">Телефон:</p>
                        <p>+7 (843) 333 75 85</p>
                        <p class="txt-grey">E-mail:</p>
                        <p>pervoedelo@gmail.com</p>
                        <p>Центр работает в будни, с 9:00 до 18:00</p>
                    </div>
                    <div class="col-sm-6 sm-hide">
                        <div class="map">
                            <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A96acf0bced4a4f4bf602b8f6493e9d12ecac2ed9a7eec6453ddd0273e56451ed&amp;height=600&amp;lang=ru_RU&amp;scroll=true"></script>
                        </div>
                    </div>
                </div>
            </div>
        </article>


		
	</section>


	<footer class="sm-hide">
        <div class="menu_main_line ">
            <div class="menu_left-block">
                <a href="#header" class="navbar-menu_logo soft">
                    <object type="image/svg+xml" data="public/img/LOGO/LOGO.svg" >
                        <img src="public/img/LOGO/LOGO.png" alt="" />
                    </object>
                </a>

                <div class="menu_main_line_nav sm-hide">
                    <ul>
                        <li><a href="#" class="soft-link">Главная</a> </li>
                        <li><a href="#art-block-3" class="soft-link">Секции</a> </li>
                        <li><a href="#art-block-5" class="soft-link">Курсы</a> </li>
                        <li><a href="#art-block-8" class="soft-link">Стоимость обучения</a> </li>
                        <li><a href="#art-block-11" class="soft-link">Тренеры</a> </li>
                        <li><a href="#art-block-14" class="soft-link">Контакты</a> </li>
                    </ul>
                </div>
            </div>
        </div>

	</footer>

	<script src="public/js/main.min.js?01" type="text/javascript"></script>

</body>

</html>
