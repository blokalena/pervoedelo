$(document).ready(function(){

    //плавная прокрутка до якоря
    $('a.soft-link[href^="#"]').click(function (event) {
        event.preventDefault();
        var el = $(this).attr('href');
        $('html, body').animate({ scrollTop: $(el).offset().top-100}, 500); //offset to menu

        $('#menuModal').hide();

        // return false;
    });

    //меню для мобильной версии
    $('.navbar-toggle').click(function(){
        navtoggle();
    });

    function navtoggle() {
        $('.navbar-toggle').toggleClass('collapsed');
        $('.navbar-collapse').toggleClass('xs-opened');
    }

    tabMenuBlocks();
    accordion();
    rightMenu();

    $('.js-modal-btn').click(function () {
        var modalName  = $(this).data('modal');
        $('#'+modalName).show();
    })


    $('.js-modal-close').click(function () {
        var modalName  = $(this).data('modal');
        $('#'+modalName).hide();
    });

        //slick
    $('.js-slikc-slider-1').slick({
        dots: false,
        speed: 1000,
        centerMode: true,
        centerPadding:'25%',
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000,
        cssEase: 'ease-in',
        infinite: true,
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true,
                    centerMode: false
                }
            }
        ]
    });

    $('.js-slikc-slider').slick({
        dots: false,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: true,
        centerPadding: '25%',
        autoplay: false,
        autoplaySpeed: 4000,
        cssEase: 'ease-in',
        infinite: true,
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: false,
                    centerPadding: '0',
                    infinite: true,
                    dots: true
                }
            }
        ]
    });



    $('.js-phone-mask').mask('+9 (999) 999-99-99');


});

function tabMenuBlocks() {
    var tabItems = $('.multi-tab_tab-menu > li');
    var horMenu = $('.multi-tab_hor-menu > li');
​
    for (var i = 0; i < tabItems.length; i++) {
        $(tabItems[i]).click(function () {
            for (var q = 0; q < tabItems.length; q++) {
                $(tabItems[q]).removeClass('active');
                $('.multi-tab-content').removeClass('active');
            }
​
            var row = $(this).attr('class');
​
            $(this).addClass('active');
            $('#'+row+'.multi-tab-content').addClass('active');
        });
    }
​
    for (var i = 0; i < horMenu.length; i++) {
        $(horMenu[i]).click(function () {
            for (var q = 0; q < horMenu.length; q++) {
                $(horMenu[q]).removeClass('active');
                $('.tab-hor-content').removeClass('active');
            }
​
            var row = $(this).attr('class');
​
            $(this).addClass('active');
            $('.'+row+'.tab-hor-content').addClass('active');
        });
    }
}

function rightMenu() {
    var tabItems = $('.contact-modal_tabs_ico');

    $('.js-modal-contact-close').click(function () {
        $('.contact-modal-win').addClass('hide');
        $(tabItems).parent().removeClass('opened');
    })
​
    for (var i = 0; i < tabItems.length; i++) {
        $(tabItems[i]).click(function () {
            if($('.contact-modal-win').hasClass('hide')){
                $('.contact-modal-win').removeClass('hide');
                $(tabItems).parent().addClass('opened');
            }
            var modal = $(this).data('tab');
            for (var q = 0; q < tabItems.length; q++) {
                $(tabItems[q]).removeClass('active');
                $('.contact-modal_content').removeClass('open');
            }
​
            $(this).addClass('active');
            $('.contact-modal_content_'+modal).addClass('open');
        });
    }

    $('.js-open-win').click(function () {
        var localmodal = $(this).data('tab');
        for (var q = 0; q < tabItems.length; q++) {
            $(tabItems[q]).removeClass('active');
            $('.contact-modal_content').removeClass('open');
        }
        $('.contact-modal_tabs_ico.ico-'+localmodal).addClass('active');
        $('.contact-modal_content_'+localmodal).addClass('open');
        $(tabItems).parent().addClass('opened');
        $('.contact-modal-win').removeClass('hide');
    })


}

function accordion() {
    var tabItems = $('.accordion_title');
    for (var i = 0; i < tabItems.length; i++) {

        $(tabItems[i]).click(function () {

            if($(this).parent().hasClass('active')){
                $(this).parent().removeClass('active');
                return;
            }

            for (var q = 0; q < tabItems.length; q++) {
                $(tabItems[q]).parent().removeClass('active');
            }
            $(this).parent().addClass('active');
        });
    }

}
